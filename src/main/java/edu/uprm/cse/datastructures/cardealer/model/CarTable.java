package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.Map;

public class CarTable {
	//instance of a circular sorted doubly linked list
	private static Map<Long, Car> carTable = new HashTableOA<Long, Car>(50);
	
	//returns an instance of the list
	public static Map<Long, Car> getInstance() {
		return carTable;
	}
	
	//empties the list
	public static void resetCars() {
		((HashTableOA<Long, Car>) carTable).clear();
	}
}
