package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	//Class that creates a node
	private static class Node<E>{

		//class variables
		private E element;
		private Node<E> prev, next;

		//constructors
		public Node() {
			this.element = null;
			this.next = this.prev = null; 
		}

		public Node(E e, Node<E> n, Node<E> p) {
			element = e;
			prev = p; 
			next = n;
		}

		//getters and setters
		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}	
	}

	//class variables
	private Node<E> header; 
	private int currentSize;
	private Comparator<E> comparator;

	//constructor
	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.currentSize = 0;
		this.header = new Node<>();

		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comparator = comparator;


	}

	//method that returns an iterator
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}


	//method that adds an object to the list
	public boolean add(E obj) {

		//Node node is the node to add to the list
		Node<E> temp = this.header.getNext();
		Node<E> node = new Node<E>(obj, null, null);


		while(temp != this.header) {

			//compares which is smaller in order to find the position to add node
			if(comparator.compare(node.getElement(), temp.getElement()) < 0) {

				//adds node
				node.setNext(temp);
				node.setPrev(temp.getPrev());
				temp.getPrev().setNext(node);
				temp.setPrev(node);
				this.currentSize++;
				return true;

			}

			//continues iterating until a position is found
			temp = temp.getNext();

		}

		//adds node 
		node.setNext(temp);
		node.setPrev(temp.getPrev());
		temp.getPrev().setNext(node);
		temp.setPrev(node);

		this.currentSize++;	

		return true;

	}

	//returns the size of the list
	public int size() {
		return this.currentSize;
	}

	//removes a specified object
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		}
		else {
			this.remove(i);
			return true;
		}
	}

	//removes an object at an specific position
	public boolean remove(int index) {
		//if the index is not within boundaries of list
		if((index < 0) || (index >=  this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		else {

			//target node is the  one to be removed 
			Node<E> temp = this.header;
			int currentPosition = 0;
			Node<E> target = null;

			//iterates looking for the given index
			while(currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}

			//removes the node from he list, and reorganizes connections
			target = temp.getNext();
			target.getNext().setPrev(temp);
			temp.setNext(target.getNext());
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
			this.currentSize--;
			return true;
		}
	}

	//removes all the copies of a given object and returns the amount of 
	//times it was removed
	public int removeAll(E obj) {
		int count = 0;
		//updates a count every time the object is removes
		while(this.remove(obj)){
			count++;
		}

		return count;
	}

	//returns the first element of a list
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getNext().element;
	}

	//returns the last element of a list
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	//gets the element and a given position
	public E get(int index) {
		//checks index is within boundaries of the list
		if ((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		else {
			//node at first position
			Node<E> temp = header.getNext();
			int currentPosition = 0;
			//iterates through list until index is found, updates temp
			while(currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}
			return temp.getElement();
		}
	}

	//empties the list
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	//checks if an element is on the list
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	//checks if the list is empty
	public boolean isEmpty() {
		return this.size() == 0;
	}

	//finds the position in which an element appears for the fist time
	public int firstIndex(E e) {
		int i = 0;
		//iterates through list
		for (Node<E> temp = this.header.getNext(); temp.getElement() != null; temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// if not found
		return -1;
	}

	//finds the last position in which an element appears
	public int lastIndex(E e) {
		int i = this.currentSize - 1;
		//iterated through list
		for (Node<E> temp = this.header.getPrev(); temp.getElement() != null; temp = temp.getPrev(), --i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// if not found
		return -1;
	}

	//iterator class
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		//class variables
		private Node<E> nextNode;

		//constructor
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		//checks if a node has a next one
		public boolean hasNext() {
			return nextNode != header;
		}

		//returns the next node 
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} 
			else {
				throw new NoSuchElementException();
			}
		}


	}

}