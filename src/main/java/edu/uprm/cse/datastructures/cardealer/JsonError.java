package edu.uprm.cse.datastructures.cardealer;

public class JsonError {
	//class variables
	private String type;
	private String message;
	
	//constructor
	public JsonError(String type, String message){
		this.type = type;
		this.message = message;                
	} 
	
	//getters and setters
	public String getType(){
		return this.type;
	}

	public String getMessage(){
		return this.message;
	} 

}
