package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;;

public class CarComparator implements Comparator<Car> {
	//method that compares two cars and returns 1 if car 1 > car 2,
	//0 if car 1 = car 2, and -1 if car 1 < car 2
	public int compare(Car car1, Car car2) {
		//compares car brand
		if(car1.getCarBrand().compareTo(car2.getCarBrand()) == 0) {
			//compares car model
			if (car1.getCarModel().compareTo(car2.getCarModel()) == 0) {
				//compares car model option
				if(car1.getCarModelOption().compareTo(car2.getCarModelOption())  == 0) {
					return 0;
				}
				return car1.getCarModelOption().compareTo(car2.getCarModelOption());
			}
			return car1.getCarModel().compareTo(car2.getCarModel());
		}	
		return car1.getCarBrand().compareTo(car2.getCarBrand());
	}
}
