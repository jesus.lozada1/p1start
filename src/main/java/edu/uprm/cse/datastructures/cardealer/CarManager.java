package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	//this method returns all of the cars in the inventory 
	public Car[] allCars(){
		//array of cars
		Car[] cars = new Car[CarTable.getInstance().size()];
		SortedList<Car> SC = CarTable.getInstance().getValues();
		//iterates through list adding all cars to array
		for(long i = 0; i < SC.size(); i++) {
			cars[(int) i] = SC.get((int) i);
		}
		return cars;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	//this method returns a car with a given id
	public Car getCar(@PathParam("id") long id) {
		//iterates through list finding a car 
		for(long i = 0; i < CarTable.getInstance().size(); ++i) {
			//compares car ids to a given id
			if(CarTable.getInstance().get(i).getCarId() == id) { 
				return CarTable.getInstance().get(i);
			}
		}
	    throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	//this method adds a car to the list
	public Response addCar(Car car) {
		CarTable.getInstance().put(car.getCarId(), car);
		//returns a 201 Created if car is added to the list
		return Response.status(201).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	//this method updates a car with a given ID
	public Response updateCar(@PathParam("id") long id, Car car) {
		//iterates list finding a car
		for(long i = 0; i < CarTable.getInstance().size(); ++i) {
			//compares car ids to given id
			if(CarTable.getInstance().get(i).getCarId() == id) {
				//removes the car and adds the updated one, returns 200 OK if updated
				CarTable.getInstance().remove(id);
				CarTable.getInstance().put(id, car);
				return Response.status(200).build();				
			}
		}
		//returns 404 Not Found if there is no car with given ID
		return Response.status(404).build();		
	}

	@DELETE
	@Path("{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	//deletes a car from the list
	public Response deleteCar(@PathParam("id") long id) {
		//iterates through list finding a car with given id
		for(long i = 0; i < CarTable.getInstance().size(); i++) {
			//compares car ids with given id
			if(CarTable.getInstance().get(i).getCarId() == id) {
				//removes car and returns 200 OK if deleted
				CarTable.getInstance().remove(i);
				return Response.status(200).build();
			}
		}
		//returns 404 not found if there is no car in list with given id
		return Response.status(404).build();
	}



}
