package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class HashTableOA<K, V> implements Map<K,V>{

	public static class MapEntry<K, V>{
		 private K key;
		 private V value;
		 private boolean available;
		 
		 public MapEntry(K key, V value, boolean available) {
			 super();
			 this.key = key;
			 this.value = value;
			 this.available = available;
		 }
		 
		 public K getKey() {
			 return key;
		 }
		 
		 public void setKey(K key) {
			 this.key = key;
		 }
		 
		 public V getValue() {
			 return value;
		 }
		 
		 public void setValue(V value) {
			 this.value = value;
		 }
		 
		 public boolean isAvailable() {
			 return available;
		 }
	}
	
	private int currentSize;
	private List<Object> buckets;
	private CarComparator comparator;
	
	private int hashFunction(K key) {
		return (key.hashCode() * 5) % this.buckets.size();
	}
	
	private int doubleHash(K key, int target) {
		return (int) (((Math.pow(this.hashFunction(key), 2)) + target) % this.buckets.size());
	}
	
	
	
	public HashTableOA(int initialCapacity) {
		this.currentSize = 0;
		this.buckets = new ArrayList<Object>(initialCapacity);
		
		for(int i = 0; i < initialCapacity; i++) {
			 this.buckets.add(new MapEntry<K, V>(null, null, true));
		}
	}
	
	@Override
	public int size() {
		return this.size();
	}
	

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		if(key == null) {
			throw new IllegalArgumentException("Key cannot be null in order to get value");
		}
		
		int target = this.hashFunction(key) % this.buckets.size();
		MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(target);
		
		if(M.getKey() == null){
			return null;
		}
		else if(M.getKey().equals(key)) {
			return M.getValue();
		}
		else {
			int newTarget = this.doubleHash(key, target);
			MapEntry<K, V> M2 = (MapEntry<K, V>) this.buckets.get(newTarget);
			
			if(M.getKey().equals(key)) {
				return M.getValue();
			}
			else {
				return this.getLinearProbing(key, newTarget);
			}
		}
	}
	
	private V getLinearProbing(K key, int target) {
		for(int i = (target + 1) % this.buckets.size(); i != target;
				i = (i + 1) % this.buckets.size()) {
			MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(i);
			if(M.getKey().equals(key)) {
				return M.getValue();
			}
		}	
		return null;
	}

	@Override
	public V put(K key, V value) {
		if(key == null) {
			throw new IllegalArgumentException("Key cannot be null in order to put value");
		}
		
		if(this.size() == this.buckets.size() - 1) {
			this.reallocate();
		}
		
		
		int target = this.hashFunction(key) % this.buckets.size();
		MapEntry<K,V> M = (MapEntry<K, V>) this.buckets.get(target);
		V result = M.getValue();
		
		if(M.getKey() == key) {
			M.setValue(value);
			return result;
		}
		
		if(M.isAvailable()) {
			M.setKey(key);
			M.setValue(value);
			M.available = false;
			this.currentSize++;
			return result;
			
		} 
		else {
			int newTarget = this.doubleHash(key, target);
			MapEntry<K,V> M2 = (MapEntry<K, V>) this.buckets.get(newTarget);
			V result2 = M2.getValue();
			
			if(M2.isAvailable()) {
				M2.setKey(key);
				M2.setValue(value);
				M2.available = false;
				this.currentSize++;
				return result2;
				
			} else {
				return this.putLinearProbing(key, value, newTarget);
			}
			
		}
}
	
	private void reallocate() {
		HashTableOA<K,V> newHT = new HashTableOA<K,V>(this.buckets.size() * 2);
		for(int i = 0; i < this.size(); i++) {
			MapEntry<K,V> M = (MapEntry<K, V>) this.buckets.get(i);
			if(M.getKey() != null) {
				newHT.put(M.getKey(), M.getValue());
			}
		}
	}

	private V putLinearProbing(K key, V value, int target) {
		for(int i = (target + 1) % this.buckets.size(); i != target;
				i = (i + 1) % this.buckets.size()) {
			MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(i);
			V result = M.getValue();
			if(M.isAvailable()) {
				M.setKey(key);
				M.setValue(value);
				M.available = false;
				this.currentSize++;
				return result;
			}
		}
		
		return null;
		
	}

	@Override
	public V remove(K key) {
		if(key == null) {
			throw new IllegalArgumentException("Key cannot be null in order to remove value");
		}
		
		int target = this.hashFunction(key) % this.buckets.size();
		MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(target);
		V result = M.getValue();
		
		if(!M.isAvailable() && M.getKey().equals(key)) {
			M.setKey(null);
			M.setValue(null);
			M.available = true;
			this.currentSize--;
			
			return result;
		}
		else {
			int newTarget = this.doubleHash(key, target);
			MapEntry<K, V> M2 = (MapEntry<K, V>) this.buckets.get(newTarget);
			V result2 = M2.getValue();
			
			if(!M2.isAvailable() && M.getKey().equals(key)) {
				M2.setKey(null);
				M2.setValue(null);
				M2.available = true;
				this.currentSize--;
				
				return result;
			}
			else {
				return this.removeLinearProbing(key, newTarget);
			}
		}
		
	}
	

	private V removeLinearProbing(K key, int newTarget) {
		for(int i = (newTarget + 1) % this.buckets.size(); i != newTarget;
				i = (i + 1) % this.buckets.size()) {
			MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(i);
			V result = M.getValue();
			
			if(!M.isAvailable() && M.getKey().equals(key)) {
				M.setKey(null);
				M.setValue(null);
				M.available = true;
				this.currentSize--;
				
				return result;
			}
		}	
		return null;
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) == null;
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> L = new CircularSortedDoublyLinkedList<K>((Comparator<K>) comparator);
		for(int i = 0; i < this.buckets.size(); i++) {
			MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(i);
			if(M.getKey() != null) {
				L.add(M.getKey());
			}
		}
		return L;
	}

	@Override
	public SortedList<V> getValues() {
		SortedList<V> L = new CircularSortedDoublyLinkedList<V>((Comparator<V>) comparator);
		for(int i = 0; i < this.buckets.size(); i++) {
			MapEntry<K, V> M = (MapEntry<K, V>) this.buckets.get(i);
			if(M.getValue() != null) {
				L.add(M.getValue());
			}
		}
		return L;
	}

	public void clear() {
		this.currentSize = 0;
		
		for(int i = 0; i < this.buckets.size(); ++i) {
			MapEntry<K, V> M = (MapEntry<K,V>) this.buckets.get(i);
			M.key = null;
			M.value = null;
			M.available = true;
		}
	}

}


